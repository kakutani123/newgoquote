package push_func

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"newgoquote/database"
	"newgoquote/exchange/binance"
	"newgoquote/pkg/setting"
	"newgoquote/utils/logger"
	"os"
	"os/signal"
	"strconv"
	"time"

	"newgoquote/strategy_test_market_data/baseStruct"

	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
)

var Logger *logrus.Entry
var CurrentDir string

func init() {
	setting.Setup()
	Logger = logger.NewTerminalLogger(setting.StrategySetting.LogSavePath+"/short_term.log", logger.DebugLevel, logrus.Fields{"package": "main"})
}

type Push_Template struct {
	ExchangeName  string `json:"exchangeName"`
	SpotSymbol    string `json:"spot_symbol"`
	UmSwapSymbol  string `json:"um_swap_symbol"`
	CmSwapSymbol  string `json:"cm_swap_symbol"`
	RestartHour   int    `json:"restartHour"`
	RestartMinute int    `json:"restartMinute"`
	redisClient   *redis.Client
	mysqlClient   *sql.DB
	UmB2Data      *baseStruct.BBO2Tick
	CmB2Data      *baseStruct.BBO2Tick
}

type UmSwapTicker struct {
	//E  string `json:"e"`
	//U  int    `json:"u"`
	E1 int64  `json:"E"`
	T  int64  `json:"T"`
	S  string `json:"s"`
	B  string `json:"b"`
	B1 string `json:"B"`
	A  string `json:"a"`
	A1 string `json:"A"`
}

type SwapBBOTicker struct {
	//E  string `json:"e"`
	//U  int    `json:"u"`
	E1 int64  `json:"E"`
	T  int64  `json:"T"`
	S  string `json:"s"`
	B  string `json:"b"`
	B1 string `json:"B"`
	A  string `json:"a"`
	A1 string `json:"A"`
}

type CmSwapTicker struct {
	//E  string `json:"e"`
	//U  int    `json:"u"`
	S string `json:"s"`
	//Ps string `json:"ps"`
	B  string `json:"b"`
	B1 string `json:"B"`
	A  string `json:"a"`
	A1 string `json:"A"`
	T  int64  `json:"T"`
	E1 int64  `json:"E"`
}

type SpotTicker struct {
	//E  string `json:"e"`
	E1 int64  `json:"E"`
	S  string `json:"s"`
	//P  string `json:"p"`
	//P1 string `json:"P"`
	//W  string `json:"w"`
	//X  string `json:"x"`
	//C  string `json:"c"`
	//Q  string `json:"Q"`
	B  string `json:"b"`
	B1 string `json:"B"`
	A  string `json:"a"`
	A1 string `json:"A"`
	//O  string `json:"o"`
	//H  string `json:"h"`
	//L  string `json:"l"`
	V  string `json:"v"`
	Q1 string `json:"q"`
	//O1 int    `json:"O"`
	//C1 int    `json:"C"`
	//F  int    `json:"F"`
	//L1 int    `json:"L"`
	//N  int    `json:"n"`
}

func NewPushTemplate(
	exchangename string,
	spotsymbol string,
	umswapsymbol string,
	cmswapsymbol string,
	restarthour int,
	restartminute int,
) *Push_Template {
	return &Push_Template{
		ExchangeName:  exchangename,
		SpotSymbol:    spotsymbol,
		UmSwapSymbol:  umswapsymbol,
		CmSwapSymbol:  cmswapsymbol,
		RestartHour:   restarthour,
		RestartMinute: restartminute,
		UmB2Data:      baseStruct.NewBBO2Tick(umswapsymbol, 1000, 100, 800),
		CmB2Data:      baseStruct.NewBBO2Tick(cmswapsymbol, 1000, 100, 800),
	}
}

func (self *Push_Template) calBBO(symbol string) {
	//重置条件
	fmt.Printf("---- cal bbo symbol : %s -----", symbol)
}

func (self *Push_Template) onUmSwapBbo(symbol string, event interface{}) {
	Logger.Infof("UmSwap symbol:", symbol, "event", event)
	jsonData, err := json.Marshal(event)
	if err != nil {
		Logger.Debug("Failed to marshal data:", err)
	}
	var data SwapBBOTicker
	err = json.Unmarshal(jsonData, &data)
	if err != nil {
		Logger.Debug("Failed to Unmarshal data:", err)
		return
	}
	// bbo UmSwap 数据
	AP1, _ := strconv.ParseFloat(data.A, 64)
	BP1, _ := strconv.ParseFloat(data.B, 64)
	AV1, _ := strconv.ParseFloat(data.A1, 64)
	BV1, _ := strconv.ParseFloat(data.B1, 64)
	mixp := (AP1 + BP1) / 2.0
	self.UmB2Data.UpdateTick(data.T, mixp, AP1, BP1, AV1, BV1)
	// 更新pair对数据
	if self.CmB2Data.UpdateTime < self.UmB2Data.UpdateTime {
		self.CmB2Data.UpdatePlace(data.T)
	}
	// 条件更新
	self.calBBO(symbol)
}

func (self *Push_Template) onCmSwapBbo(symbol string, event interface{}) {
	Logger.Infof("CmSwap symbol:", symbol, "event", event)
	jsonData, err := json.Marshal(event)
	if err != nil {
		Logger.Debug("Failed to marshal data:", err)
	}
	var data SwapBBOTicker
	err = json.Unmarshal(jsonData, &data)
	if err != nil {
		Logger.Debug("Failed to Unmarshal data:", err)
	}
	// bbo CmSwap 数据
	AP1, _ := strconv.ParseFloat(data.A, 64)
	BP1, _ := strconv.ParseFloat(data.B, 64)
	AV1, _ := strconv.ParseFloat(data.A1, 64)
	BV1, _ := strconv.ParseFloat(data.B1, 64)
	mixp := (AP1 + BP1) / 2.0
	self.CmB2Data.UpdateTick(data.T, mixp, AP1, BP1, AV1, BV1)
	// 更新pair对数据
	if self.UmB2Data.UpdateTime < self.CmB2Data.UpdateTime {
		self.UmB2Data.UpdatePlace(data.T)
	}
	// 条件更新
	self.calBBO(symbol)
}
func (self *Push_Template) onSpotBbo(symbol string, event interface{}) {
	Logger.Infof("spot symbol:", symbol, "event", event)
}

func (self *Push_Template) SubUmSwapBbo(RestartHour int, RestartMinute int, restart_second int) {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		err := binance.SubscribeUmSwapBbo(ctx, self.UmSwapSymbol, self.onUmSwapBbo)
		if err != nil {
			Logger.Debug("SubUmSwapAllBbo Error:", err)
		}
	}()
	for {
		now := time.Now()
		fmt.Println(RestartHour, RestartMinute, restart_second)
		next := time.Date(now.Year(), now.Month(), now.Day(), RestartHour, RestartMinute, restart_second, 0, now.Location())
		fmt.Println(next)
		if now.After(next) {
			next = next.Add(24 * time.Hour)
		}
		t := time.NewTimer(next.Sub(now))
		<-t.C
		cancel()
		ctx, cancel = context.WithCancel(context.Background())
		go func() {
			err := binance.SubscribeUmSwapBbo(ctx, self.UmSwapSymbol, self.onUmSwapBbo)
			if err != nil {
				Logger.Debug("SubscribeUmSwapBbo Error:", err)
			}
		}()
	}
}

func (self *Push_Template) SubCmSwapBbo(RestartHour int, RestartMinute int, restart_second int) {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		err := binance.SubscribeCmSwapBbo(ctx, self.CmSwapSymbol, self.onCmSwapBbo)
		if err != nil {
			Logger.Debug("SubUmSwapAllBbo Error:", err)
		}
	}()
	for {
		now := time.Now()
		fmt.Println(RestartHour, RestartMinute, restart_second)
		next := time.Date(now.Year(), now.Month(), now.Day(), RestartHour, RestartMinute, restart_second, 0, now.Location())
		fmt.Println(next)
		if now.After(next) {
			next = next.Add(24 * time.Hour)
		}
		t := time.NewTimer(next.Sub(now))
		<-t.C
		cancel()
		ctx, cancel = context.WithCancel(context.Background())
		go func() {
			err := binance.SubscribeCmSwapBbo(ctx, self.CmSwapSymbol, self.onCmSwapBbo)
			if err != nil {
				Logger.Debug("SubscribeUmSwapBbo Error:", err)
			}
		}()
	}
}

func (self *Push_Template) SubSpotBbo(RestartHour int, RestartMinute int, restart_second int) {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		err := binance.SubscribeSpotBbo(ctx, self.SpotSymbol, self.onSpotBbo)
		if err != nil {
			Logger.Debug("SubscribeSpotBbo Error:", err)
		}
	}()
	for {
		now := time.Now()
		next := time.Date(now.Year(), now.Month(), now.Day(), RestartHour, RestartMinute, restart_second, 0, now.Location())
		if now.After(next) {
			next = next.Add(24 * time.Hour)
		}
		t := time.NewTimer(next.Sub(now))
		<-t.C
		cancel()
		ctx, cancel = context.WithCancel(context.Background())
		go func() {
			err := binance.SubscribeSpotBbo(ctx, self.SpotSymbol, self.onSpotBbo)
			if err != nil {
				Logger.Debug("SubscribeSpotBbo Error:", err)
			}
		}()
	}
}

func (self *Push_Template) InitNewPushTemplate() {
	self.redisClient, _ = database.Connect_myredis()
	self.mysqlClient, _ = database.Connect_mysql()
	go self.SubSpotBbo(self.RestartHour, self.RestartMinute, 9)
	go self.SubUmSwapBbo(self.RestartHour, self.RestartMinute, 10)
	go self.SubCmSwapBbo(self.RestartHour, self.RestartMinute, 11)

}

func (self *Push_Template) RunUntilStop() {
	var ch = make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)
	<-ch
	Logger.Warningf("Notify Interrupt Signal, Prepare To Exit Strategy")
}
