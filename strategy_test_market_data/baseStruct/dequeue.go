package baseStruct

import (
	"sync"
)

// 使用泛型类型T定义dequeue
type dequeue[T any] struct {
	MaxLen int
	queue  []T
	lock   sync.Mutex
}

// Push方法添加一个元素到queue，且遵守MaxLen规则
func (q *dequeue[T]) Push(v T) {
	q.lock.Lock()
	defer q.lock.Unlock()

	if len(q.queue) >= q.MaxLen {
		// 删除第一个元素，并添加新元素到队尾
		q.queue = append(q.queue[1:], v)
	} else {
		// 直接添加新元素到队尾
		q.queue = append(q.queue, v)
	}
}

// Pop方法从queue前端移除一个元素
func (q *dequeue[T]) Pop() T {
	q.lock.Lock()
	defer q.lock.Unlock()

	var v T
	if len(q.queue) == 0 {
		return v // 返回类型T的零值
	}

	v = q.queue[0]
	q.queue = q.queue[1:]
	return v
}

// Len方法返回queue当前长度
func (q *dequeue[T]) Len() int {
	q.lock.Lock()
	defer q.lock.Unlock()
	return len(q.queue)
}

// Assign方法在queue不为空时给最后一个元素赋新值
func (q *dequeue[T]) Assign(value T) {
	q.lock.Lock()
	defer q.lock.Unlock()

	if len(q.queue) > 0 {
		q.queue[len(q.queue)-1] = value
	} else {
		// 处理queue为空的情况
		q.queue = append(q.queue, value)
	}
}

// Index方法根据索引取值
func (q *dequeue[T]) Index(i int) T {
	q.lock.Lock()
	defer q.lock.Unlock()

	var v T
	if len(q.queue) == 0 {
		return v // 返回类型T的零值
	}

	if i >= len(q.queue) || i < -len(q.queue) {
		panic("queue index out of range")
	}
	// go 不支持直接负向索引
	if i < 0 {
		return q.queue[len(q.queue)-i]
	} else {
		return q.queue[i]
	}
}

// Values方法返回当前queue的一个切片副本
func (q *dequeue[T]) Values() []T {
	q.lock.Lock()
	defer q.lock.Unlock()
	return append([]T(nil), q.queue...)
}

/*
// 1.18版本以下的接口
type dequeue struct {
	MaxLen int
	queue  []interface{}
	lock   sync.Mutex
	wait   sync.WaitGroup
}

func (q *dequeue) Push(v interface{}) {

	q.lock.Lock()
	defer q.lock.Unlock()

	if len(q.queue) >= q.MaxLen {
		q.queue = append(q.queue[1:], v)
	} else {
		q.queue = append(q.queue, v)
	}
}

func (q *dequeue) Pop() interface{} {
	q.lock.Lock()
	defer q.lock.Unlock()

	if len(q.queue) == 0 {
		return nil
	}

	v := q.queue[0]
	q.queue = q.queue[1:]
	return v
}

func (q *dequeue) Len() int {
	q.lock.Lock()
	defer q.lock.Unlock()
	return len(q.queue)
}

func (q *dequeue) Assign(value float64) {
	q.lock.Lock()
	defer q.lock.Unlock()

	if len(q.queue) > 0 {
		q.queue[len(q.queue)-1] = value
	} else {
		// 处理 dequeue 为空的情况
		q.queue = append(q.queue, value)
	}
}

func (q *dequeue) Index(i int) interface{} {
	if len(q.queue) == 0 {
		return 0.0
	}

	if i > len(q.queue) || i < -1*len(q.queue) {
		panic("quue index out of range")
	}
	return q.queue[i]
}

func (q *dequeue) Values() []interface{} {
	return q.queue
}
*/
