package baseStruct

type SymbolStruct struct {
	Symbol             string
	PricePrecision     int
	QuantiltyPrecision int
}

func NewSymbolStruct(symbol string, pricePrecision int, quantilyPrecision int) *SymbolStruct {
	return &SymbolStruct{
		Symbol:             symbol,
		PricePrecision:     pricePrecision,
		QuantiltyPrecision: quantilyPrecision,
	}
}

type TickData struct {
	//tick
	TS int64
	Lp float64
	Ap float64
	Bp float64
	Av float64
	Bv float64
}

type klineData struct {
	// k 线数据
	TS int64
	H  float64
	L  float64
	O  float64
	C  float64
	//tick 数据
	Ap float64
	Bp float64
	Av float64
	Bv float64
}

type BBO2Tick struct {
	name         string
	UpdateTime_k int64
	UpdateTime   int64
	k            int64 //多少毫秒更新一次k线
	t            int64
	Ap           float64
	Bp           float64
	Av           float64
	Bv           float64
	Mixp         float64
	TimeStp      int64
	// tick更新.
	Q_Tick   *dequeue[*TickData]
	Q_Price  *dequeue[float64]
	Q_Kline  *dequeue[float64] //K彘集合
	Q_Trans  *dequeue[float64]
	Q_KTrans *dequeue[float64]
	Price_t  float64
	Price_k  float64
	// kData    *klineData
	B_Tick *TickData
}

func NewBBO2Tick(exchName string, k int64, ticklen int, klen int) *BBO2Tick {
	return &BBO2Tick{
		name:    exchName,
		k:       k,
		t:       100,
		Q_Tick:  &dequeue[*TickData]{MaxLen: ticklen},
		Q_Price: &dequeue[float64]{MaxLen: ticklen},
		Q_Kline: &dequeue[float64]{MaxLen: klen},
	}
}

// func (BK *BBO2Tick) updateKLine(ts int64, Mixp float64, Ap float64, Bp float64, Av float64, Bv float64) {
// 	mod_timestamp := ts / BK.k
// 	if BK.UpdateTime == 0 || mod_timestamp != BK.UpdateTime {
// 		BK.Q_Kline.Push(Mixp)
// 		BK.UpdateTime = mod_timestamp
// 	} else {
// 		BK.Q_Kline = Mixp
// 	}
// }

func (BK *BBO2Tick) UpdateTick(ts int64, Mixp float64, Ap float64, Bp float64, Av float64, Bv float64) int64 {
	BK.TimeStp = ts
	BK.Mixp = Mixp
	BK.Ap = Ap
	BK.Bp = Bp
	BK.Av = Av
	BK.Bv = Bv
	// 更新Tick与Kline
	mod_timestamp := ts / BK.t
	if BK.UpdateTime == 0 || mod_timestamp != BK.UpdateTime {
		// t_ran := Mixp - BK.Q_Tick.Index(-1).(float64)
		BK.B_Tick = &TickData{TS: ts, Lp: Mixp, Ap: Ap, Bp: Bp, Av: Av, Bv: Bv}
		BK.Q_Tick.Push(BK.B_Tick) //tick
		BK.Q_Price.Push(Mixp)     //price 用于计算Eup,Cup指标
		// 涨跌幅
		if BK.Q_Tick.Len() > 1 {
			t_ran := (Mixp - BK.Price_t) / BK.Price_t
			BK.Q_Trans.Push(t_ran)
		} else {
			BK.Q_Trans.Push(0.0)
		}
		BK.UpdateTime = mod_timestamp
		BK.Price_t = Mixp
	} else {
		BK.B_Tick.Ap = Ap
		BK.B_Tick.Bp = Bp
		BK.B_Tick.Av = Av
		BK.B_Tick.Bv = Bv
		BK.B_Tick.Lp = Mixp

		BK.Q_Price.Assign(Mixp)
		BK.Q_Trans.Assign((Mixp - BK.Price_t) / BK.Price_t)
	}
	//更新K
	mod_timestamp_k := ts / BK.k
	if BK.UpdateTime_k == 0 || mod_timestamp_k != BK.UpdateTime_k {
		BK.Q_Kline.Push(Mixp)
		//涨跌幅
		if BK.Q_Kline.Len() > 1 {
			k_ran := (Mixp - BK.Price_k) / BK.Price_k
			BK.Q_KTrans.Push(k_ran)
		} else {
			BK.Q_KTrans.Push(0)
		}
		BK.UpdateTime_k = mod_timestamp_k
		BK.Price_k = Mixp

	} else {
		BK.Q_Kline.Assign(Mixp)
		BK.Q_KTrans.Assign((Mixp - BK.Price_k) / BK.Price_k)
	}
	return int64(0)
}

func (BK *BBO2Tick) UpdatePlace(ts int64) {
	BK.UpdateTick(ts, BK.Mixp, BK.Ap, BK.Bp, BK.Av, BK.Bv)
}

func (BK *BBO2Tick) update(ts int64, Mixp float64, Ap float64, Bp float64, Av float64, Bv float64) {
	BK.UpdateTick(ts, Mixp, Ap, Bp, Av, Bv)
	// BK.updateKLine(ts, Mixp, Mixp, Mixp, Mixp, Mixp)
}

// 重载 [] 运算符
// func (BK *BBO2Tick) Index(i int) float64 {
// 	if i > (BK.Q_Tick.Len()) || i < -1*(BK.Q_Tick.Len()) {
// 		panic("BBO2Tick index out of range")
// 	}
// 	value := BK.Q_Tick.Index(i)
// 	return value
// }
