package baseStruct

import (
	"fmt"
	"sync"
	"time"
)

type SafeQueue struct {
	queue  []interface{}
	lock   sync.Mutex
	wait   sync.WaitGroup
	maxLen int
}

func (q *SafeQueue) Put(item interface{}) {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.queue = append(q.queue, item)
	if len(q.queue) > q.maxLen {
		q.queue = q.queue[1:]
		q.wait.Done()
	} else {
		q.wait.Add(1)
	}
}

func (q *SafeQueue) Get() interface{} {
	q.lock.Lock()
	defer q.lock.Unlock()
	if len(q.queue) == 0 {
		q.wait.Wait()
	}
	item := q.queue[0]
	q.queue = q.queue[1:]
	return item
}

func (q *SafeQueue) Start() {
	q.wait.Add(1)
	go func() {
		for {
			item := q.Get()
			if item != nil {
				fmt.Println(item)
			}
			time.Sleep(100 * time.Millisecond)
		}
	}()
}

func main() {
	q := &SafeQueue{maxLen: 5}
	q.Start()

	for i := 0; i < 10; i++ {
		q.Put(i)
		time.Sleep(50 * time.Millisecond)
	}

	time.Sleep(5 * time.Second)
}
