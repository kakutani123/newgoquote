package logger

import (
	"time"

	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

const (
	PanicLevel = logrus.PanicLevel
	FatalLevel = logrus.FatalLevel
	ErrorLevel = logrus.ErrorLevel
	WarnLevel  = logrus.WarnLevel
	InfoLevel  = logrus.InfoLevel
	DebugLevel = logrus.DebugLevel
	TraceLevel = logrus.TraceLevel
)

func NewTerminalLogger(filename string, level logrus.Level, fields logrus.Fields) *logrus.Entry {
	// 设置时区为"Asia/Shanghai"
	time.Local, _ = time.LoadLocation("Asia/Shanghai")
	log := logrus.New()
	logFile := &lumberjack.Logger{
		Filename:   filename,
		MaxSize:    10,   // 每个日志文件的最大大小，以MB为单位
		MaxBackups: 20,   // 最大备份文件数
		MaxAge:     2,    // 最大保存天数
		Compress:   true, // 是否启用压缩
	}
	log.SetOutput(logFile)
	log.SetLevel(level)
	var f = &logrus.TextFormatter{
		DisableColors:   true,
		TimestampFormat: time.RFC3339Nano,
		DisableQuote:    true,
	}
	log.SetFormatter(f)
	return log.WithFields(fields)
}
