module newgoquote

go 1.21.1

require (
	github.com/go-ini/ini v1.67.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.7.1
	github.com/gorilla/websocket v1.5.0
	github.com/sirupsen/logrus v1.9.3
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)

require (
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.27.10 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
