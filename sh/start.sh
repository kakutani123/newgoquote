#!/bin/bash
# sh sh/start.sh strategy_use_system task_1 dev 
strategy_name=$1
task_name=$2
strategy_env=$3

if [ -z "$strategy_name" ]; then
  echo "脚本没有传入第一个参数：策略名"
  exit 1
fi

if [ -z "$task_name" ]; then
  echo "脚本没有传入第二个参数：任务名"
  exit 1
fi

if [ "$strategy_env" != "dev" ] && [ "$strategy_env" != "prod" ]; then
  echo "脚本没有传入第三个参数：环境名(dev或prod)"
  exit 1
fi


# 获取脚本所在的目录路径
script_dir="$(cd "$(dirname "$0")" && pwd)"

# 切换到脚本所在的目录
cd "$script_dir"

# 切换到策略所在的根目录
cd "../"

# 获取当前目录
current_dir=$(pwd)

if [ ! -d "$current_dir/$strategy_name" ]; then
  echo "目录不存在：$current_dir/$strategy_name"
  exit 1
fi

# 切换到策略所在的目录
cd "$strategy_name"

# 获取当前目录
current_dir=$(pwd)

# 打印当前目录
echo "当前目录：$current_dir"

log_path=$current_dir/logs/$task_name
if [ ! -d "$log_path" ]; then
  mkdir -p "$log_path"
fi
log_path=$log_path/long_term.log
if [ ! -e "$log_path" ]; then
    echo "logs/$task_name/long_term.log 文件已创建." > $log_path
fi

git pull >>$log_path 2>&1

build_folder=$current_dir/build
if [ ! -d "$build_folder" ]; then
  mkdir -p "$build_folder"
  echo "build文件夹已创建"
fi

logs_folder=$current_dir/logs/$task_name
if [ ! -d "$logs_folder" ]; then
  mkdir -p "$logs_folder"
  echo "logs/$task_name 文件夹已创建"
fi

conf_folder=$current_dir/conf
if [ ! -d "$conf_folder" ]; then
  mkdir -p "$conf_folder"
  echo "conf文件夹已创建"
fi

# 设置开发环境
cp ../conf/app.$strategy_env.ini  conf/app.ini

proccess_path=$current_dir/build/$task_name
# 查询指定路径的进程ID，并杀死
pids=$(pgrep -f "$proccess_path")
if [ -n "$pids" ]; then
    # 使用循环逐个杀死进程
    for pid in $pids; do
        echo "进程已被杀死: $pid"
        kill "$pid"
    done
fi

# 编译 Go 代码
go build -o build/$task_name main.go >>$log_path 2>&1

# 后台运行可执行文件，并将输出重定向到日志文件
nohup $current_dir/build/$task_name $task_name >>$log_path 2>&1 &

# 获取命令的退出状态
status=$?
# 检查命令是否成功执行
if [ $status -eq 0 ]; then
  # 获取Go程序的PID
  pid=$!
  echo "进程启动成功:$pid"
  exit 0
else
  echo "进程启动失败"
  exit 1
fi

# 查询指定路径的进程ID
# pids=$(pgrep -f "$proccess_path")
# if [ -n "$pids" ]; then
#   echo "进程启动成功:$pids"
#   exit 0
# else
#   echo "进程启动失败"
#   exit 1
# fi