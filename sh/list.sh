#!/bin/bash

# sh sh/list.sh
# sh sh/list.sh strategy_use_system
strategy_name=$1

# 获取脚本所在的目录路径
script_dir="$(cd "$(dirname "$0")" && pwd)"

# 切换到脚本所在的目录
cd "$script_dir"

# 切换到策略所在的根目录
cd "../"

# 获取当前目录
current_dir=$(pwd)

proccess_path=$current_dir/$strategy_name
if [ -z "$strategy_name" ]; then
  proccess_path=$current_dir
fi

pids=$(ps -ef | grep "$proccess_path")
echo $pids