package binance

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	pm_url_accountinfo                 = "/papi/v1/account"
	pm_url_accountbalance              = "/papi/v1/balance"
	pm_url_spot_order                  = "/papi/v1/margin/order"
	pm_url_umswap_order                = "/papi/v1/um/order"
	pm_url_cmswap_order                = "/papi/v1/cm/order"
	pm_url_query_all_spot_open_order   = "/papi/v1/margin/openOrders"
	pm_url_query_all_umswap_open_order = "/papi/v1/um/openOrders"
	pm_url_query_all_cmswap_open_order = "/papi/v1/cm/openOrders"
	pm_url_query_spot_history_order    = "/papi/v1/margin/order"
	pm_url_query_umswap_history_order  = "/papi/v1/um/order"
	pm_url_query_cmswap_history_order  = "/papi/v1/cm/order"
	pm_url_cancel_spot_history_order   = "/papi/v1/margin/order"
	pm_url_cancel_umswap_history_order = "/papi/v1/um/order"
	pm_url_cancel_cmswap_history_order = "/papi/v1/cm/order"
	pm_url_change_umswap_leverage      = "/papi/v1/um/leverage"
	pm_url_change_cmswap_leverage      = "/papi/v1/cm/leverage"
	pm_url_bnb_tranfer                 = "/papi/v1/bnb-transfer"
	pm_url_umswap_info                 = "/papi/v1/um/account"
	pm_url_cmswap_info                 = "/papi/v1/cm/account"
	pm_url_repay                       = "/papi/v1/repay-futures-negative-balance"
	pm_url_auto_collection             = "/papi/v1/auto-collection"
)

type Http_Template struct {
	ApiKey    string `json:"apiKey"`
	SecretKey string `json:"secretKey"`
	Client    *http.Client
}

func NewHttpTemplate(
	api_key string,
	secret_key string,
) *Http_Template {
	return &Http_Template{
		ApiKey:    api_key,
		SecretKey: secret_key,
	}
}

type PMApiConfigTemplate struct {
	ApiKey    string `json:"apiKey"`
	SecretKey string `json:"secretKey"`
}

type AccountInfo struct {
	UniMMR                   string `json:"uniMMR"`                   // 统一账户维持保证金率
	AccountEquity            string `json:"accountEquity"`            // 以USD计价的账户权益
	ActualEquity             string `json:"actualEquity"`             // 不考虑质押率后的以USD计价账户权益
	AccountInitialMargin     string `json:"accountInitialMargin"`     // 以USD计价的账户初始保证金
	AccountMaintMargin       string `json:"accountMaintMargin"`       // 以USD计价统一账户维持保证金
	AccountStatus            string `json:"accountStatus"`            // 统一账户账户状态
	VirtualMaxWithdrawAmount string `json:"virtualMaxWithdrawAmount"` // 以USD计价的最大可转出
	TotalAvailableBalance    string `json:"totalAvailableBalance"`    // 总可用余额
	TotalMarginOpenLoss      string `json:"totalMarginOpenLoss"`      // 总开仓亏损
	UpdateTime               int64  `json:"updateTime"`               // 更新时间
}

type AccountBalance struct {
	Asset               string `json:"asset"`
	TotalWalletBalance  string `json:"totalWalletBalance"`
	CrossMarginAsset    string `json:"crossMarginAsset"`
	CrossMarginBorrowed string `json:"crossMarginBorrowed"`
	CrossMarginFree     string `json:"crossMarginFree"`
	CrossMarginInterest string `json:"crossMarginInterest"`
	CrossMarginLocked   string `json:"crossMarginLocked"`
	UmWalletBalance     string `json:"umWalletBalance"`
	UmUnrealizedPNL     string `json:"umUnrealizedPNL"`
	CmWalletBalance     string `json:"cmWalletBalance"`
	CmUnrealizedPNL     string `json:"cmUnrealizedPNL"`
	UpdateTime          int64  `json:"updateTime"`
	NegativeBalance     string `json:"negativeBalance"`
}

type SpotOrder struct {
	Symbol                  string `json:"symbol"`
	OrderId                 int    `json:"orderId"`
	ClientOrderId           string `json:"clientOrderId"`
	TransactTime            int64  `json:"transactTime"`
	Price                   string `json:"price"`
	SelfTradePreventionMode string `json:"selfTradePreventionMode"`
	OrigQty                 string `json:"origQty"`
	ExecutedQty             string `json:"executedQty"`
	CummulativeQuoteQty     string `json:"cummulativeQuoteQty"`
	Status                  string `json:"status"`
	TimeInForce             string `json:"timeInForce"`
	Type                    string `json:"type"`
	Side                    string `json:"side"`
	MarginBuyBorrowAmount   int    `json:"marginBuyBorrowAmount"`
	MarginBuyBorrowAsset    string `json:"marginBuyBorrowAsset"`
	Fills                   []struct {
		Price           string `json:"price"`
		Qty             string `json:"qty"`
		Commission      string `json:"commission"`
		CommissionAsset string `json:"commissionAsset"`
	} `json:"fills"`
}

type UmSwapOrder struct {
	ClientOrderId           string `json:"clientOrderId"`
	CumQty                  string `json:"cumQty"`
	CumQuote                string `json:"cumQuote"`
	ExecutedQty             string `json:"executedQty"`
	OrderId                 int    `json:"orderId"`
	AvgPrice                string `json:"avgPrice"`
	OrigQty                 string `json:"origQty"`
	Price                   string `json:"price"`
	ReduceOnly              bool   `json:"reduceOnly"`
	Side                    string `json:"side"`
	PositionSide            string `json:"positionSide"`
	Status                  string `json:"status"`
	Symbol                  string `json:"symbol"`
	TimeInForce             string `json:"timeInForce"`
	Type                    string `json:"type"`
	SelfTradePreventionMode string `json:"selfTradePreventionMode"`
	GoodTillDate            int64  `json:"goodTillDate"`
	UpdateTime              int64  `json:"updateTime"`
}

type CmSwapOrder struct {
	ClientOrderId string `json:"clientOrderId"`
	CumQty        string `json:"cumQty"`
	CumBase       string `json:"cumBase"`
	ExecutedQty   string `json:"executedQty"`
	OrderId       int    `json:"orderId"`
	AvgPrice      string `json:"avgPrice"`
	OrigQty       string `json:"origQty"`
	Price         string `json:"price"`
	ReduceOnly    bool   `json:"reduceOnly"`
	Side          string `json:"side"`
	PositionSide  string `json:"positionSide"`
	Status        string `json:"status"`
	Symbol        string `json:"symbol"`
	Pair          string `json:"pair"`
	TimeInForce   string `json:"timeInForce"`
	Type          string `json:"type"`
	UpdateTime    int64  `json:"updateTime"`
}

type SpotOpenOrder struct {
	ClientOrderId           string `json:"clientOrderId"`
	CummulativeQuoteQty     string `json:"cummulativeQuoteQty"`
	ExecutedQty             string `json:"executedQty"`
	IcebergQty              string `json:"icebergQty"`
	IsWorking               bool   `json:"isWorking"`
	OrderId                 int64  `json:"orderId"`
	OrigQty                 string `json:"origQty"`
	Price                   string `json:"price"`
	Side                    string `json:"side"`
	Status                  string `json:"status"`
	StopPrice               string `json:"stopPrice"`
	Symbol                  string `json:"symbol"`
	Time                    int64  `json:"time"`
	TimeInForce             string `json:"timeInForce"`
	Type                    string `json:"type"`
	UpdateTime              int64  `json:"updateTime"`
	AccountId               int64  `json:"accountId"`
	SelfTradePreventionMode string `json:"selfTradePreventionMode"`
}

type UmSwapOpenOrder struct {
	AvgPrice                string `json:"avgPrice"`
	ClientOrderId           string `json:"clientOrderId"`
	CumQuote                string `json:"cumQuote"`
	ExecutedQty             string `json:"executedQty"`
	OrderId                 int    `json:"orderId"`
	OrigQty                 string `json:"origQty"`
	OrigType                string `json:"origType"`
	Price                   string `json:"price"`
	ReduceOnly              bool   `json:"reduceOnly"`
	Side                    string `json:"side"`
	PositionSide            string `json:"positionSide"`
	Status                  string `json:"status"`
	Symbol                  string `json:"symbol"`
	Time                    int64  `json:"time"`
	TimeInForce             string `json:"timeInForce"`
	Type                    string `json:"type"`
	UpdateTime              int64  `json:"updateTime"`
	SelfTradePreventionMode string `json:"selfTradePreventionMode"`
	GoodTillDate            int64  `json:"goodTillDate"`
}

type CmSwapOpenOrder struct {
	AvgPrice      string `json:"avgPrice"`
	ClientOrderId string `json:"clientOrderId"`
	CumBase       string `json:"cumBase"`
	ExecutedQty   string `json:"executedQty"`
	OrderId       int    `json:"orderId"`
	OrigQty       string `json:"origQty"`
	OrigType      string `json:"origType"`
	Price         string `json:"price"`
	ReduceOnly    bool   `json:"reduceOnly"`
	Side          string `json:"side"`
	PositionSide  string `json:"positionSide"`
	Status        string `json:"status"`
	Symbol        string `json:"symbol"`
	Pair          string `json:"pair"`
	Time          int64  `json:"time"`
	TimeInForce   string `json:"timeInForce"`
	Type          string `json:"type"`
	UpdateTime    int64  `json:"updateTime"`
}

type SpotHistoryOrder struct {
	ClientOrderID           string  `json:"clientOrderId"`
	CummulativeQuoteQty     string  `json:"cummulativeQuoteQty"`
	ExecutedQty             string  `json:"executedQty"`
	IcebergQty              string  `json:"icebergQty"`
	IsWorking               bool    `json:"isWorking"`
	OrderID                 int     `json:"orderId"`
	OrigQty                 string  `json:"origQty"`
	Price                   string  `json:"price"`
	Side                    string  `json:"side"`
	Status                  string  `json:"status"`
	StopPrice               string  `json:"stopPrice"`
	Symbol                  string  `json:"symbol"`
	Time                    int64   `json:"time"`
	TimeInForce             string  `json:"timeInForce"`
	Type                    string  `json:"type"`
	UpdateTime              int64   `json:"updateTime"`
	AccountID               int     `json:"accountId"`
	SelfTradePreventionMode string  `json:"selfTradePreventionMode"`
	PreventedMatchID        *int    `json:"preventedMatchId"`
	PreventedQuantity       *string `json:"preventedQuantity"`
}

type UmSwapHistoryOrder struct {
	AvgPrice                string `json:"avgPrice"`
	ClientOrderId           string `json:"clientOrderId"`
	CumQuote                string `json:"cumQuote"`
	ExecutedQty             string `json:"executedQty"`
	OrderId                 int64  `json:"orderId"`
	OrigQty                 string `json:"origQty"`
	OrigType                string `json:"origType"`
	Price                   string `json:"price"`
	ReduceOnly              bool   `json:"reduceOnly"`
	Side                    string `json:"side"`
	PositionSide            string `json:"positionSide"`
	Status                  string `json:"status"`
	Symbol                  string `json:"symbol"`
	Time                    int64  `json:"time"`
	TimeInForce             string `json:"timeInForce"`
	Type                    string `json:"type"`
	UpdateTime              int64  `json:"updateTime"`
	SelfTradePreventionMode string `json:"selfTradePreventionMode"`
	GoodTillDate            int64  `json:"goodTillDate"`
}

type CmSwapHistoryOrder struct {
	AvgPrice      string `json:"avgPrice"`
	ClientOrderID string `json:"clientOrderId"`
	CumBase       string `json:"cumBase"`
	ExecutedQty   string `json:"executedQty"`
	OrderID       int    `json:"orderId"`
	OrigQty       string `json:"origQty"`
	OrigType      string `json:"origType"`
	Price         string `json:"price"`
	ReduceOnly    bool   `json:"reduceOnly"`
	Side          string `json:"side"`
	Status        string `json:"status"`
	Symbol        string `json:"symbol"`
	Pair          string `json:"pair"`
	PositionSide  string `json:"positionSide"`
	Time          int64  `json:"time"`
	TimeInForce   string `json:"timeInForce"`
	Type          string `json:"type"`
	UpdateTime    int64  `json:"updateTime"`
}

type CancelSpotOrderRes struct {
	Symbol                  string `json:"symbol"`
	OrderId                 int    `json:"orderId"`
	OrigClientOrderId       string `json:"origClientOrderId"`
	ClientOrderId           string `json:"clientOrderId"`
	Price                   string `json:"price"`
	OrigQty                 string `json:"origQty"`
	ExecutedQty             string `json:"executedQty"`
	CummulativeQuoteQty     string `json:"cummulativeQuoteQty"`
	Status                  string `json:"status"`
	TimeInForce             string `json:"timeInForce"`
	Type                    string `json:"type"`
	Side                    string `json:"side"`
	SelfTradePreventionMode string `json:"selfTradePreventionMode"`
}

type CancelUmSwapOrderRes struct {
	AvgPrice                string `json:"avgPrice"`
	ClientOrderId           string `json:"clientOrderId"`
	CumQty                  string `json:"cumQty"`
	CumQuote                string `json:"cumQuote"`
	ExecutedQty             string `json:"executedQty"`
	OrderId                 int64  `json:"orderId"`
	OrigQty                 string `json:"origQty"`
	Price                   string `json:"price"`
	ReduceOnly              bool   `json:"reduceOnly"`
	Side                    string `json:"side"`
	PositionSide            string `json:"positionSide"`
	Status                  string `json:"status"`
	Symbol                  string `json:"symbol"`
	TimeInForce             string `json:"timeInForce"`
	Type                    string `json:"type"`
	UpdateTime              int64  `json:"updateTime"`
	SelfTradePreventionMode string `json:"selfTradePreventionMode"`
	GoodTillDate            int64  `json:"goodTillDate"`
}

type CancelCmSwapOrderRes struct {
	AvgPrice      string `json:"avgPrice"`
	ClientOrderId string `json:"clientOrderId"`
	CumQty        string `json:"cumQty"`
	CumBase       string `json:"cumBase"`
	ExecutedQty   string `json:"executedQty"`
	OrderId       int64  `json:"orderId"`
	OrigQty       string `json:"origQty"`
	Price         string `json:"price"`
	ReduceOnly    bool   `json:"reduceOnly"`
	Side          string `json:"side"`
	PositionSide  string `json:"positionSide"`
	Status        string `json:"status"`
	Symbol        string `json:"symbol"`
	Pair          string `json:"pair"`
	TimeInForce   string `json:"timeInForce"`
	Type          string `json:"type"`
	UpdateTime    int64  `json:"updateTime"`
}

type ChangeUmSwapLeverageRes struct {
	Leverage         int    `json:"leverage"`
	MaxNotionalValue string `json:"maxNotionalValue"`
	Symbol           string `json:"symbol"`
}

type ChangeCmSwapLeverageRes struct {
	Leverage int    `json:"leverage"`
	MaxQty   string `json:"maxQty"`
	Symbol   string `json:"symbol"`
}

type BNBTransferRes struct {
	TranId int `json:"tranId"`
}

type UmSwapAsset struct {
	Asset                  string `json:"asset"`
	CrossWalletBalance     string `json:"crossWalletBalance"`
	CrossUnPnl             string `json:"crossUnPnl"`
	MaintMargin            string `json:"maintMargin"`
	InitialMargin          string `json:"initialMargin"`
	PositionInitialMargin  string `json:"positionInitialMargin"`
	OpenOrderInitialMargin string `json:"openOrderInitialMargin"`
	UpdateTime             int64  `json:"updateTime"`
}

type UmSwapPosition struct {
	Symbol                 string `json:"symbol"`
	InitialMargin          string `json:"initialMargin"`
	MaintMargin            string `json:"maintMargin"`
	UnrealizedProfit       string `json:"unrealizedProfit"`
	PositionInitialMargin  string `json:"positionInitialMargin"`
	OpenOrderInitialMargin string `json:"openOrderInitialMargin"`
	Leverage               string `json:"leverage"`
	EntryPrice             string `json:"entryPrice"`
	MaxNotional            string `json:"maxNotional"`
	BidNotional            string `json:"bidNotional"`
	AskNotional            string `json:"askNotional"`
	PositionSide           string `json:"positionSide"`
	PositionAmt            string `json:"positionAmt"`
	UpdateTime             int64  `json:"updateTime"`
	BreakEvenPrice         string `json:"breakEvenPrice"`
}

type UmSwapInfo struct {
	TradeGroupId int              `json:"tradeGroupId"`
	Assets       []UmSwapAsset    `json:"assets"`
	Positions    []UmSwapPosition `json:"positions"`
}

type CmAsset struct {
	Asset                  string `json:"asset"`
	CrossWalletBalance     string `json:"crossWalletBalance"`
	CrossUnPnl             string `json:"crossUnPnl"`
	MaintMargin            string `json:"maintMargin"`
	InitialMargin          string `json:"initialMargin"`
	PositionInitialMargin  string `json:"positionInitialMargin"`
	OpenOrderInitialMargin string `json:"openOrderInitialMargin"`
	UpdateTime             int64  `json:"updateTime"`
}

type CmPosition struct {
	Symbol                 string `json:"symbol"`
	PositionAmt            string `json:"positionAmt"`
	InitialMargin          string `json:"initialMargin"`
	MaintMargin            string `json:"maintMargin"`
	UnrealizedProfit       string `json:"unrealizedProfit"`
	PositionInitialMargin  string `json:"positionInitialMargin"`
	OpenOrderInitialMargin string `json:"openOrderInitialMargin"`
	Leverage               string `json:"leverage"`
	PositionSide           string `json:"positionSide"`
	EntryPrice             string `json:"entryPrice"`
	MaxQty                 string `json:"maxQty"`
	UpdateTime             int64  `json:"updateTime"`
	BreakEvenPrice         string `json:"breakEvenPrice"`
}

type CmSwapInfo struct {
	Assets    []CmAsset    `json:"assets"`
	Positions []CmPosition `json:"positions"`
}

type RepayRes struct {
	Msg string `json:"msg"`
}

type AutoCollectionRes struct {
	Msg string `json:"msg"`
}

func (self *Http_Template) InitHttpTemplate() {
	self.Client = &http.Client{Transport: &http.Transport{DisableKeepAlives: false}}
}

func (self *Http_Template) QueryAccountInfo() (AccountInfo, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	// 发送HTTP请求
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_accountinfo+"?"+params.Encode(), nil)
	if err != nil {
		return AccountInfo{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return AccountInfo{}, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return AccountInfo{}, err
	}
	if resp.StatusCode != 200 {
		return AccountInfo{}, err
	}
	var data AccountInfo
	if err := json.Unmarshal(body, &data); err != nil {
		return AccountInfo{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryAccountBalance() ([]AccountBalance, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_accountbalance+"?"+params.Encode(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, err
	}
	var data []AccountBalance
	if err := json.Unmarshal(body, &data); err != nil {
		return nil, err
	}
	return data, nil
}

func (self *Http_Template) GenerateUid() string {
	ns := time.Now().UnixNano()
	rand_nu := rand.Intn(10000)
	return fmt.Sprintf("x-%v%v", ns, rand_nu)
}

func (self *Http_Template) PlaceSpotOrder(symbol string,
	side string, ordertype string, quantity string, price string,
	newClientOrderId string, timeInForce string) (SpotOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("side", side)
	params.Set("quantity", quantity)
	params.Set("price", price)
	params.Set("type", ordertype)
	params.Set("newClientOrderId", newClientOrderId)
	params.Set("timeInForce", timeInForce)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_spot_order+"?"+params.Encode(), nil)
	if err != nil {
		return SpotOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return SpotOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return SpotOrder{}, err
	}
	if resp.StatusCode != 200 {
		return SpotOrder{}, err
	}
	var data SpotOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return SpotOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) PlaceUmSwapOrder(symbol string,
	side string, ordertype string, quantity string, price string,
	newClientOrderId string, timeInForce string) (UmSwapOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("side", side)
	params.Set("quantity", quantity)
	params.Set("price", price)
	params.Set("type", ordertype)
	params.Set("newClientOrderId", newClientOrderId)
	params.Set("timeInForce", timeInForce)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_umswap_order+"?"+params.Encode(), nil)
	if err != nil {
		return UmSwapOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return UmSwapOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return UmSwapOrder{}, err
	}
	if resp.StatusCode != 200 {
		return UmSwapOrder{}, err
	}
	var data UmSwapOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return UmSwapOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) PlaceCmSwapOrder(symbol string,
	side string, ordertype string, quantity string, price string,
	newClientOrderId string, timeInForce string) (CmSwapOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("side", side)
	params.Set("quantity", quantity)
	params.Set("price", price)
	params.Set("type", ordertype)
	params.Set("newClientOrderId", newClientOrderId)
	params.Set("timeInForce", timeInForce)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_cmswap_order+"?"+params.Encode(), nil)
	if err != nil {
		return CmSwapOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return CmSwapOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return CmSwapOrder{}, err
	}
	if resp.StatusCode != 200 {
		return CmSwapOrder{}, err
	}
	var data CmSwapOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return CmSwapOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryAllSpotOpenOrder() ([]SpotOpenOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_query_all_spot_open_order+"?"+params.Encode(), nil)
	if err != nil {
		return []SpotOpenOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return []SpotOpenOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []SpotOpenOrder{}, err
	}
	if resp.StatusCode != 200 {
		return []SpotOpenOrder{}, err
	}
	var data []SpotOpenOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return []SpotOpenOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryAllUmSwapOpenOrder() ([]UmSwapOpenOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_query_all_umswap_open_order+"?"+params.Encode(), nil)
	if err != nil {
		return []UmSwapOpenOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return []UmSwapOpenOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []UmSwapOpenOrder{}, err
	}
	if resp.StatusCode != 200 {
		return []UmSwapOpenOrder{}, err
	}
	var data []UmSwapOpenOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return []UmSwapOpenOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryAllCmSwapOpenOrder() ([]CmSwapOpenOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_query_all_cmswap_open_order+"?"+params.Encode(), nil)
	if err != nil {
		return []CmSwapOpenOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return []CmSwapOpenOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []CmSwapOpenOrder{}, err
	}
	if resp.StatusCode != 200 {
		return []CmSwapOpenOrder{}, err
	}
	var data []CmSwapOpenOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return []CmSwapOpenOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) QuerySpotHistoryOrder(symbol string, origClientOrderId string) (SpotHistoryOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("origClientOrderId", origClientOrderId)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_query_spot_history_order+"?"+params.Encode(), nil)
	if err != nil {
		return SpotHistoryOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return SpotHistoryOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return SpotHistoryOrder{}, err
	}
	if resp.StatusCode != 200 {
		return SpotHistoryOrder{}, err
	}
	var data SpotHistoryOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return SpotHistoryOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryUmSwapHistoryOrder(symbol string, origClientOrderId string) (UmSwapHistoryOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("origClientOrderId", origClientOrderId)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_query_umswap_history_order+"?"+params.Encode(), nil)
	if err != nil {
		return UmSwapHistoryOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return UmSwapHistoryOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return UmSwapHistoryOrder{}, err
	}
	if resp.StatusCode != 200 {
		return UmSwapHistoryOrder{}, err
	}
	var data UmSwapHistoryOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return UmSwapHistoryOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryCmSwapHistoryOrder(symbol string, origClientOrderId string) (CmSwapHistoryOrder, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("origClientOrderId", origClientOrderId)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_query_cmswap_history_order+"?"+params.Encode(), nil)
	if err != nil {
		return CmSwapHistoryOrder{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return CmSwapHistoryOrder{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return CmSwapHistoryOrder{}, err
	}
	if resp.StatusCode != 200 {
		return CmSwapHistoryOrder{}, err
	}
	var data CmSwapHistoryOrder
	if err := json.Unmarshal(body, &data); err != nil {
		return CmSwapHistoryOrder{}, err
	}
	return data, nil
}

func (self *Http_Template) CancelSpotOrder(symbol string, origClientOrderId string) (CancelSpotOrderRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("origClientOrderId", origClientOrderId)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("DELETE", pm_http_apiBaseUrl+pm_url_cancel_spot_history_order+"?"+params.Encode(), nil)
	if err != nil {
		return CancelSpotOrderRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return CancelSpotOrderRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return CancelSpotOrderRes{}, err
	}
	if resp.StatusCode != 200 {
		return CancelSpotOrderRes{}, err
	}
	var data CancelSpotOrderRes
	if err := json.Unmarshal(body, &data); err != nil {
		return CancelSpotOrderRes{}, err
	}
	return data, nil
}

func (self *Http_Template) CancelUmSwapOrder(symbol string, origClientOrderId string) (CancelUmSwapOrderRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("origClientOrderId", origClientOrderId)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("DELETE", pm_http_apiBaseUrl+pm_url_cancel_umswap_history_order+"?"+params.Encode(), nil)
	if err != nil {
		return CancelUmSwapOrderRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return CancelUmSwapOrderRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return CancelUmSwapOrderRes{}, err
	}
	if resp.StatusCode != 200 {
		return CancelUmSwapOrderRes{}, err
	}
	var data CancelUmSwapOrderRes
	if err := json.Unmarshal(body, &data); err != nil {
		return CancelUmSwapOrderRes{}, err
	}
	return data, nil
}

func (self *Http_Template) CancelCmSwapOrder(symbol string, origClientOrderId string) (CancelCmSwapOrderRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("origClientOrderId", origClientOrderId)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("DELETE", pm_http_apiBaseUrl+pm_url_cancel_cmswap_history_order+"?"+params.Encode(), nil)
	if err != nil {
		return CancelCmSwapOrderRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return CancelCmSwapOrderRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return CancelCmSwapOrderRes{}, err
	}
	if resp.StatusCode != 200 {
		return CancelCmSwapOrderRes{}, err
	}
	var data CancelCmSwapOrderRes
	if err := json.Unmarshal(body, &data); err != nil {
		return CancelCmSwapOrderRes{}, err
	}
	return data, nil
}

func (self *Http_Template) ChangeUmSwapLeverage(symbol string, leverage int) (ChangeUmSwapLeverageRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("leverage", strconv.Itoa(leverage))
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_change_umswap_leverage+"?"+params.Encode(), nil)
	if err != nil {
		return ChangeUmSwapLeverageRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return ChangeUmSwapLeverageRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return ChangeUmSwapLeverageRes{}, err
	}
	if resp.StatusCode != 200 {
		return ChangeUmSwapLeverageRes{}, err
	}
	var data ChangeUmSwapLeverageRes
	if err := json.Unmarshal(body, &data); err != nil {
		return ChangeUmSwapLeverageRes{}, err
	}
	return data, nil
}

func (self *Http_Template) ChangeCmSwapLeverage(symbol string, leverage int) (ChangeCmSwapLeverageRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("leverage", strconv.Itoa(leverage))
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_change_cmswap_leverage+"?"+params.Encode(), nil)
	if err != nil {
		return ChangeCmSwapLeverageRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return ChangeCmSwapLeverageRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return ChangeCmSwapLeverageRes{}, err
	}
	if resp.StatusCode != 200 {
		return ChangeCmSwapLeverageRes{}, err
	}
	var data ChangeCmSwapLeverageRes
	if err := json.Unmarshal(body, &data); err != nil {
		return ChangeCmSwapLeverageRes{}, err
	}
	return data, nil
}

func (self *Http_Template) BnbTransfer(amount string, transferSide string) (BNBTransferRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("amount", amount)
	params.Set("transferSide", transferSide)
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_bnb_tranfer+"?"+params.Encode(), nil)
	if err != nil {
		return BNBTransferRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp.body:", resp.Body)
	if err != nil {
		return BNBTransferRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return BNBTransferRes{}, err
	}
	if resp.StatusCode != 200 {
		return BNBTransferRes{}, err
	}
	var data BNBTransferRes
	if err := json.Unmarshal(body, &data); err != nil {
		return BNBTransferRes{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryUmSwapInfo() (UmSwapInfo, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_umswap_info+"?"+params.Encode(), nil)
	if err != nil {
		return UmSwapInfo{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp:", resp)
	if err != nil {
		return UmSwapInfo{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return UmSwapInfo{}, err
	}
	if resp.StatusCode != 200 {
		return UmSwapInfo{}, err
	}
	var data UmSwapInfo
	if err := json.Unmarshal(body, &data); err != nil {
		fmt.Println(999, err)
		return UmSwapInfo{}, err
	}
	return data, nil
}

func (self *Http_Template) QueryCmSwapInfo() (CmSwapInfo, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("GET", pm_http_apiBaseUrl+pm_url_cmswap_info+"?"+params.Encode(), nil)
	if err != nil {
		return CmSwapInfo{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp:", resp)
	if err != nil {
		return CmSwapInfo{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return CmSwapInfo{}, err
	}
	if resp.StatusCode != 200 {
		return CmSwapInfo{}, err
	}
	var data CmSwapInfo
	if err := json.Unmarshal(body, &data); err != nil {
		return CmSwapInfo{}, err
	}
	return data, nil
}

func (self *Http_Template) RePay() (RepayRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_repay+"?"+params.Encode(), nil)
	if err != nil {
		return RepayRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp:", resp)
	if err != nil {
		return RepayRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return RepayRes{}, err
	}
	if resp.StatusCode != 200 {
		return RepayRes{}, err
	}
	var data RepayRes
	if err := json.Unmarshal(body, &data); err != nil {
		return RepayRes{}, err
	}
	return data, nil
}

func (self *Http_Template) AutoCollect() (AutoCollectionRes, error) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	params := url.Values{}
	params.Set("timestamp", timestamp)
	params.Set("recvWindow", "5000")
	sign := hmac.New(sha256.New, []byte(self.SecretKey))
	sign.Write([]byte(params.Encode()))
	signature := hex.EncodeToString(sign.Sum(nil))
	req, err := http.NewRequest("POST", pm_http_apiBaseUrl+pm_url_repay+"?"+params.Encode(), nil)
	if err != nil {
		return AutoCollectionRes{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("X-MBX-APIKEY", self.ApiKey)
	query := req.URL.Query()
	query.Add("signature", signature)
	req.URL.RawQuery = query.Encode()
	fmt.Println("req:", req)
	resp, err := self.Client.Do(req)
	defer resp.Body.Close()
	fmt.Println("resp:", resp)
	if err != nil {
		return AutoCollectionRes{}, err
	}
	body, err := io.ReadAll(resp.Body)
	fmt.Println("body:", body)
	if err != nil {
		return AutoCollectionRes{}, err
	}
	if resp.StatusCode != 200 {
		return AutoCollectionRes{}, err
	}
	var data AutoCollectionRes
	if err := json.Unmarshal(body, &data); err != nil {
		return AutoCollectionRes{}, err
	}
	return data, nil
}
