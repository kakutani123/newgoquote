package binance

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"os"
	"os/signal"
	"time"
)

const (
	um_swap_ws_apiBaseUrl   = "wss://fstream.binance.com/ws"
	um_swap_http_apiBaseUrl = "https://fapi.binance.com"
)

type MarkPriceEvent struct {
	E  string `json:"e"`
	E1 int64  `json:"E"`
	S  string `json:"s"`
	P  string `json:"p"`
	I  string `json:"i"`
	P1 string `json:"P"`
	R  string `json:"r"`
	T  int64  `json:"T"`
}

func SubscribeUmSwapKline(ctx context.Context, symbol string, onKline func(symbol string, event interface{})) error {
	var c *websocket.Conn
	var err error
	cancel_stage := 0
	// 重连逻辑封装在一个函数中
	connect := func() error {
		u := fmt.Sprintf("%s/%s_perpetual@continuousKline_1m", um_swap_ws_apiBaseUrl, symbol)
		fmt.Printf("sub url %s \n", u)
		c, _, err = websocket.DefaultDialer.Dial(u, nil)
		if err != nil {
			return err
		}
		return nil
	}

	// 首次连接
	if err := connect(); err != nil {
		return err
	}

	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				fmt.Printf("%s, ReadMessage error: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
				if cancel_stage == 0 {
					// 尝试重连
				OuterLoop:
					for {
						if err := connect(); err != nil {
							fmt.Printf("%s, WebSocket reconnection failed: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
							time.Sleep(15 * time.Second)
						} else {
							break OuterLoop
						}
					}
					continue
				} else if cancel_stage == 1 {
					return
				}

			}
			var event interface{}
			err = json.Unmarshal(message, &event)
			onKline(symbol, event)
		}
	}()

	pongTicker := time.NewTicker(5 * time.Minute)
	defer pongTicker.Stop()

	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				fmt.Printf("write pong: %s \n", err)
				return err
			}
		case <-interrupt:
			fmt.Printf("interrupt")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done():
			fmt.Println("context canceled")
			cancel_stage = 1
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
			}
			c.Close()
			return ctx.Err()
		}
	}
}

func SubscribeUmSwapAllMarkPrice(ctx context.Context,
	onMarkPrice func(event MarkPriceEvent)) error {
	u := fmt.Sprintf("%s/!markPrice@arr@1s", um_swap_ws_apiBaseUrl)
	fmt.Println(u)

	c, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		return err
	}

	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {

			_, message, err := c.ReadMessage()
			if err != nil {
				currentTime := time.Now()
				formattedTime := currentTime.Format("2006-01-02 15:04:05")
				fmt.Printf("%s, ReadMessage error: %s \n", formattedTime, err)
				return
			}
			var messages []MarkPriceEvent
			err = json.Unmarshal(message, &messages)
			if err != nil {
				currentTime := time.Now()
				formattedTime := currentTime.Format("2006-01-02 15:04:05")
				fmt.Println(formattedTime, err)
			}
			for _, m := range messages {
				onMarkPrice(m)
			}

		}
	}()
	pongTicker := time.NewTicker(5 * time.Minute)
	defer pongTicker.Stop()
	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				log.Println("write pong:", err)
				return err
			}
		case <-interrupt:
			fmt.Println("interrupt")

			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done(): // 添加这个新分支来处理取消信号
			fmt.Println("context canceled")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
			}
			c.Close()
			return ctx.Err()
		}
	}
}

func SubscribeUmSwapAllTicker(ctx context.Context, onBbo func(event interface{})) error {
	var c *websocket.Conn
	var err error
	cancel_stage := 0
	// 重连逻辑封装在一个函数中
	connect := func() error {
		u := fmt.Sprintf("%s/!bookTicker", um_swap_ws_apiBaseUrl)
		fmt.Printf("sub url %s \n", u)
		c, _, err = websocket.DefaultDialer.Dial(u, nil)
		if err != nil {
			return err
		}
		return nil
	}

	// 首次连接
	if err := connect(); err != nil {
		return err
	}

	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				fmt.Printf("%s, ReadMessage error: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
				if cancel_stage == 0 {
					// 尝试重连
				OuterLoop:
					for {
						if err := connect(); err != nil {
							fmt.Printf("%s, WebSocket reconnection failed: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
							time.Sleep(15 * time.Second)
						} else {
							break OuterLoop
						}
					}
					continue
				} else if cancel_stage == 1 {
					return
				}

			}
			var event interface{}
			err = json.Unmarshal(message, &event)
			onBbo(event)
		}
	}()

	pongTicker := time.NewTicker(5 * time.Minute)
	defer pongTicker.Stop()

	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				fmt.Printf("write pong: %s \n", err)
				return err
			}
		case <-interrupt:
			fmt.Printf("interrupt")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done():
			fmt.Println("context canceled")
			cancel_stage = 1
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
			}
			c.Close()
			return ctx.Err()
		}
	}
}

func SubscribeUmSwapBbo(ctx context.Context, symbol string, onBbo func(symbol string, event interface{})) error {
	var c *websocket.Conn
	var err error
	cancel_stage := 0
	// 重连逻辑封装在一个函数中
	connect := func() error {
		u := fmt.Sprintf("%s/%s@bookTicker", um_swap_ws_apiBaseUrl, symbol)
		fmt.Printf("sub url %s \n", u)
		c, _, err = websocket.DefaultDialer.Dial(u, nil)
		if err != nil {
			return err
		}
		return nil
	}

	// 首次连接
	if err := connect(); err != nil {
		return err
	}

	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				fmt.Printf("%s, ReadMessage error: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
				if cancel_stage == 0 {
					// 尝试重连
				OuterLoop:
					for {
						if err := connect(); err != nil {
							fmt.Printf("%s, WebSocket reconnection failed: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
							time.Sleep(15 * time.Second)
						} else {
							break OuterLoop
						}
					}
					continue
				} else if cancel_stage == 1 {
					return
				}

			}
			var event interface{}
			err = json.Unmarshal(message, &event)
			onBbo(symbol, event)
		}
	}()

	pongTicker := time.NewTicker(5 * time.Minute)
	defer pongTicker.Stop()

	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				fmt.Printf("write pong: %s \n", err)
				return err
			}
		case <-interrupt:
			fmt.Printf("interrupt")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done():
			fmt.Println("context canceled")
			cancel_stage = 1
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
			}
			c.Close()
			return ctx.Err()
		}
	}
}
