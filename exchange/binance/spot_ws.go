package binance

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"os"
	"os/signal"
	"time"
)

const (
	spot_ws_apiBaseUrl   = "wss://stream.binance.com:9443/ws"
	spot_http_apiBaseUrl = "https://api.binance.com"
)

func SubscribeSpotKline(ctx context.Context, symbol string, onKline func(symbol string, event interface{})) error {
	var c *websocket.Conn
	var err error
	cancel_stage := 0
	// 重连逻辑封装在一个函数中
	connect := func() error {
		u := fmt.Sprintf("%s/%s@kline_1m", spot_ws_apiBaseUrl, symbol)
		fmt.Printf("sub url %s \n", u)
		c, _, err = websocket.DefaultDialer.Dial(u, nil)
		if err != nil {
			return err
		}
		return nil
	}

	// 首次连接
	if err := connect(); err != nil {
		return err
	}

	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				fmt.Printf("%s, ReadMessage error: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
				if cancel_stage == 0 {
					// 尝试重连
				OuterLoop:
					for {
						if err := connect(); err != nil {
							fmt.Printf("%s, WebSocket reconnection failed: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
							time.Sleep(15 * time.Second)
						} else {
							break OuterLoop
						}
					}
					continue
				} else if cancel_stage == 1 {
					return
				}

			}
			var event interface{}
			err = json.Unmarshal(message, &event)
			onKline(symbol, event)
		}
	}()

	pongTicker := time.NewTicker(5 * time.Minute)
	defer pongTicker.Stop()

	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				fmt.Printf("write pong: %s \n", err)
				return err
			}
		case <-interrupt:
			fmt.Printf("interrupt")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done():
			fmt.Println("context canceled")
			cancel_stage = 1
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
			}
			c.Close()
			return ctx.Err()
		}
	}
}

func SubscribeSpotAllTicker(ctx context.Context, onAllTicker func(event interface{})) error {
	var c *websocket.Conn
	var err error
	cancel_stage := 0
	// 重连逻辑封装在一个函数中
	connect := func() error {
		u := fmt.Sprintf("%s/!ticker@arr", spot_ws_apiBaseUrl)
		fmt.Printf("sub url %s \n", u)
		c, _, err = websocket.DefaultDialer.Dial(u, nil)
		if err != nil {
			return err
		}
		return nil
	}

	// 首次连接
	if err := connect(); err != nil {
		return err
	}

	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				fmt.Printf("%s, ReadMessage error: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
				if cancel_stage == 0 {
					// 尝试重连
				OuterLoop:
					for {
						if err := connect(); err != nil {
							fmt.Printf("%s, WebSocket reconnection failed: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
							time.Sleep(15 * time.Second)
						} else {
							break OuterLoop
						}
					}
					continue
				} else if cancel_stage == 1 {
					return
				}

			}
			var event interface{}
			err = json.Unmarshal(message, &event)
			onAllTicker(event)
		}
	}()

	pongTicker := time.NewTicker(5 * time.Minute)
	defer pongTicker.Stop()

	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				fmt.Printf("write pong: %s \n", err)
				return err
			}
		case <-interrupt:
			fmt.Printf("interrupt")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done():
			fmt.Println("context canceled")
			cancel_stage = 1
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
			}
			c.Close()
			return ctx.Err()
		}
	}
}

func SubscribeSpotBbo(ctx context.Context, symbol string, onBbo func(symbol string, event interface{})) error {
	var c *websocket.Conn
	var err error
	cancel_stage := 0
	// 重连逻辑封装在一个函数中
	connect := func() error {
		u := fmt.Sprintf("%s/%s@bookTicker", spot_ws_apiBaseUrl, symbol)
		fmt.Printf("sub url %s \n", u)
		c, _, err = websocket.DefaultDialer.Dial(u, nil)
		if err != nil {
			return err
		}
		return nil
	}

	// 首次连接
	if err := connect(); err != nil {
		return err
	}

	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				fmt.Printf("%s, ReadMessage error: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
				if cancel_stage == 0 {
					// 尝试重连
				OuterLoop:
					for {
						if err := connect(); err != nil {
							fmt.Printf("%s, WebSocket reconnection failed: %s \n", time.Now().Format("2006-01-02 15:04:05"), err)
							time.Sleep(15 * time.Second)
						} else {
							break OuterLoop
						}
					}
					continue
				} else if cancel_stage == 1 {
					return
				}

			}
			var event interface{}
			err = json.Unmarshal(message, &event)
			onBbo(symbol, event)
		}
	}()

	pongTicker := time.NewTicker(5 * time.Minute)
	defer pongTicker.Stop()

	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				fmt.Printf("write pong: %s \n", err)
				return err
			}
		case <-interrupt:
			fmt.Printf("interrupt")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done():
			fmt.Println("context canceled")
			cancel_stage = 1
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Println("write close:", err)
			}
			c.Close()
			return ctx.Err()
		}
	}
}
