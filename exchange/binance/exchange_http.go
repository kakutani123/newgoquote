package binance

import (
	"encoding/json"
	"io"
	"net/http"
)

type CmSwapExchangeInfoRes struct {
	ExchangeFilters []interface{} `json:"exchangeFilters"`
	RateLimits      []struct {
		Interval      string `json:"interval"`
		IntervalNum   int    `json:"intervalNum"`
		Limit         int    `json:"limit"`
		RateLimitType string `json:"rateLimitType"`
	} `json:"rateLimits"`
	ServerTime int64 `json:"serverTime"`
	Symbols    []struct {
		Filters []struct {
			FilterType     string `json:"filterType"`
			MaxPrice       string `json:"maxPrice,omitempty"`
			MinPrice       string `json:"minPrice,omitempty"`
			TickSize       string `json:"tickSize,omitempty"`
			MaxQty         string `json:"maxQty,omitempty"`
			MinQty         string `json:"minQty,omitempty"`
			StepSize       string `json:"stepSize,omitempty"`
			Limit          int    `json:"limit,omitempty"`
			MultiplierUp   string `json:"multiplierUp,omitempty"`
			MultiplierDown string `json:"multiplierDown,omitempty"`
			//MultiplierDecimal int    `json:"multiplierDecimal,omitempty"`
		} `json:"filters"`
		OrderType             []string      `json:"OrderType"`
		TimeInForce           []string      `json:"timeInForce"`
		LiquidationFee        string        `json:"liquidationFee"`
		MarketTakeBound       string        `json:"marketTakeBound"`
		Symbol                string        `json:"symbol"`
		Pair                  string        `json:"pair"`
		ContractType          string        `json:"contractType"`
		DeliveryDate          int64         `json:"deliveryDate"`
		OnboardDate           int64         `json:"onboardDate"`
		ContractStatus        string        `json:"contractStatus"`
		ContractSize          int64         `json:"contractSize"`
		QuoteAsset            string        `json:"quoteAsset"`
		BaseAsset             string        `json:"baseAsset"`
		MarginAsset           string        `json:"marginAsset"`
		PricePrecision        int           `json:"pricePrecision"`
		QuantityPrecision     int           `json:"quantityPrecision"`
		BaseAssetPrecision    int           `json:"baseAssetPrecision"`
		QuotePrecision        int           `json:"quotePrecision"`
		EqualQtyPrecision     int           `json:"equalQtyPrecision"`
		TriggerProtect        string        `json:"triggerProtect"`
		MaintMarginPercent    string        `json:"maintMarginPercent"`
		RequiredMarginPercent string        `json:"requiredMarginPercent"`
		UnderlyingType        string        `json:"underlyingType"`
		UnderlyingSubType     []interface{} `json:"underlyingSubType"`
	} `json:"symbols"`
	Timezone string `json:"timezone"`
}

type UmSwapExchangeInfoRes struct {
	ExchangeFilters []interface{} `json:"exchangeFilters"`
	RateLimits      []struct {
		Interval      string `json:"interval"`
		IntervalNum   int    `json:"intervalNum"`
		Limit         int    `json:"limit"`
		RateLimitType string `json:"rateLimitType"`
	} `json:"rateLimits"`
	ServerTime int64 `json:"serverTime"`
	Assets     []struct {
		Asset           string `json:"asset"`
		MarginAvailable bool   `json:"marginAvailable"`
	} `json:"assets"`
	Symbols []struct {
		Symbol                string   `json:"symbol"`
		Pair                  string   `json:"pair"`
		ContractType          string   `json:"contractType"`
		DeliveryDate          int64    `json:"deliveryDate"`
		OnboardDate           int64    `json:"onboardDate"`
		Status                string   `json:"status"`
		MaintMarginPercent    string   `json:"maintMarginPercent"`
		RequiredMarginPercent string   `json:"requiredMarginPercent"`
		BaseAsset             string   `json:"baseAsset"`
		QuoteAsset            string   `json:"quoteAsset"`
		MarginAsset           string   `json:"marginAsset"`
		PricePrecision        int      `json:"pricePrecision"`
		QuantityPrecision     int      `json:"quantityPrecision"`
		BaseAssetPrecision    int      `json:"baseAssetPrecision"`
		QuotePrecision        int      `json:"quotePrecision"`
		UnderlyingType        string   `json:"underlyingType"`
		UnderlyingSubType     []string `json:"underlyingSubType"`
		SettlePlan            int      `json:"settlePlan"`
		TriggerProtect        string   `json:"triggerProtect"`
		Filters               []struct {
			FilterType     string `json:"filterType"`
			MaxPrice       string `json:"maxPrice,omitempty"`
			MinPrice       string `json:"minPrice,omitempty"`
			TickSize       string `json:"tickSize,omitempty"`
			MaxQty         string `json:"maxQty,omitempty"`
			MinQty         string `json:"minQty,omitempty"`
			StepSize       string `json:"stepSize,omitempty"`
			Limit          int    `json:"limit,omitempty"`
			Notional       string `json:"notional,omitempty"`
			MultiplierUp   string `json:"multiplierUp,omitempty"`
			MultiplierDown string `json:"multiplierDown,omitempty"`
			//MultiplierDecimal int    `json:"multiplierDecimal,omitempty"`
		} `json:"filters"`
		OrderType       []string `json:"OrderType"`
		TimeInForce     []string `json:"timeInForce"`
		LiquidationFee  string   `json:"liquidationFee"`
		MarketTakeBound string   `json:"marketTakeBound"`
	} `json:"symbols"`
	Timezone string `json:"timezone"`
}

type SpotFilter struct {
	FilterType            string      `json:"filterType"`
	MinPrice              string      `json:"minPrice,omitempty"`
	MaxPrice              string      `json:"maxPrice,omitempty"`
	TickSize              string      `json:"tickSize,omitempty"`
	MinQty                string      `json:"minQty,omitempty"`
	MaxQty                string      `json:"maxQty,omitempty"`
	StepSize              string      `json:"stepSize,omitempty"`
	Limit                 int         `json:"limit,omitempty"`
	MinTrailingAboveDelta int         `json:"minTrailingAboveDelta,omitempty"`
	MaxTrailingAboveDelta int         `json:"maxTrailingAboveDelta,omitempty"`
	MinTrailingBelowDelta int         `json:"minTrailingBelowDelta,omitempty"`
	MaxTrailingBelowDelta int         `json:"maxTrailingBelowDelta,omitempty"`
	BidMultiplierUp       string      `json:"bidMultiplierUp,omitempty"`
	BidMultiplierDown     string      `json:"bidMultiplierDown,omitempty"`
	AskMultiplierUp       string      `json:"askMultiplierUp,omitempty"`
	AskMultiplierDown     string      `json:"askMultiplierDown,omitempty"`
	AvgPriceMins          int         `json:"avgPriceMins,omitempty"`
	MinNotional           string      `json:"minNotional,omitempty"`
	ApplyMinToMarket      interface{} `json:"applyMinToMarket,omitempty"`
	MaxNotional           string      `json:"maxNotional,omitempty"`
	ApplyMaxToMarket      interface{} `json:"applyMaxToMarket,omitempty"`
	MaxNumOrders          int         `json:"maxNumOrders,omitempty"`
	MaxNumAlgoOrders      int         `json:"maxNumAlgoOrders,omitempty"`
}

type SpotExchangeInfoRes struct {
	Timezone   string `json:"timezone"`
	ServerTime int64  `json:"serverTime"`
	RateLimits []struct {
	} `json:"rateLimits"`
	ExchangeFilters []interface{} `json:"exchangeFilters"`
	Symbols         []struct {
		Symbol                          string        `json:"symbol"`
		Status                          string        `json:"status"`
		BaseAsset                       string        `json:"baseAsset"`
		BaseAssetPrecision              int           `json:"baseAssetPrecision"`
		QuoteAsset                      string        `json:"quoteAsset"`
		QuotePrecision                  int           `json:"quotePrecision"`
		QuoteAssetPrecision             int           `json:"quoteAssetPrecision"`
		OrderTypes                      []string      `json:"orderTypes"`
		IcebergAllowed                  bool          `json:"icebergAllowed"`
		OcoAllowed                      bool          `json:"ocoAllowed"`
		QuoteOrderQtyMarketAllowed      bool          `json:"quoteOrderQtyMarketAllowed"`
		AllowTrailingStop               bool          `json:"allowTrailingStop"`
		IsSpotTradingAllowed            bool          `json:"isSpotTradingAllowed"`
		IsMarginTradingAllowed          bool          `json:"isMarginTradingAllowed"`
		CancelReplaceAllowed            bool          `json:"cancelReplaceAllowed"`
		Filters                         []SpotFilter  `json:"filters"`
		Permissions                     []interface{} `json:"permissions"`
		PermissionSets                  [][]string    `json:"permissionSets"`
		DefaultSelfTradePreventionMode  string        `json:"defaultSelfTradePreventionMode"`
		AllowedSelfTradePreventionModes []string      `json:"allowedSelfTradePreventionModes"`
	} `json:"symbols"`
}

func QuerySpotExchangeInfo() (SpotExchangeInfoRes, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", spot_http_apiBaseUrl+"/api/v3/exchangeInfo", nil)
	if err != nil {
		return SpotExchangeInfoRes{}, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return SpotExchangeInfoRes{}, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	//fmt.Printf("body:%s", body)
	if err != nil {
		return SpotExchangeInfoRes{}, err
	}
	var data SpotExchangeInfoRes
	err = json.Unmarshal(body, &data)
	if err != nil {
		return SpotExchangeInfoRes{}, err
	}
	return data, err
}

func QueryUmSwapExchangeInfo() (UmSwapExchangeInfoRes, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", um_swap_http_apiBaseUrl+"/fapi/v1/exchangeInfo", nil)
	if err != nil {
		return UmSwapExchangeInfoRes{}, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return UmSwapExchangeInfoRes{}, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	//fmt.Printf("body:%s", body)
	if err != nil {
		return UmSwapExchangeInfoRes{}, err
	}
	var data UmSwapExchangeInfoRes
	err = json.Unmarshal(body, &data)
	if err != nil {
		return UmSwapExchangeInfoRes{}, err
	}
	return data, err
}

func QueryCmSwapExchangeInfo() (CmSwapExchangeInfoRes, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", cm_swap_http_apiBaseUrl+"/dapi/v1/exchangeInfo", nil)
	if err != nil {
		return CmSwapExchangeInfoRes{}, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return CmSwapExchangeInfoRes{}, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	//fmt.Printf("body:%s", body)
	if err != nil {
		return CmSwapExchangeInfoRes{}, err
	}
	var data CmSwapExchangeInfoRes
	err = json.Unmarshal(body, &data)
	if err != nil {
		return CmSwapExchangeInfoRes{}, err
	}
	return data, err
}
